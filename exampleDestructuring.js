const arrayExample = [
  {
    articulo: '0010046',
    descripcion1: 'BROSIN DE 25 GRS',
    descripcion2: 'BROSIN DE 25 GRS',
    categoria: 'VETERINARIA',
    familia: null,
    grupo: null,
    claveFabricante: '0010046',
    fabricante: 'NO TIENE',
    unidad: 'pza',
    precioLista: 90,
  },
  {
    articulo: '0010106',
    descripcion1: 'CARDIOBEE 15 DE 100 ML',
    descripcion2: 'CARDIOBEE 15 100 ML',
    categoria: 'VETERINARIA',
    familia: null,
    grupo: null,
    claveFabricante: '0010106',
    fabricante: 'NO TIENE',
    unidad: 'pza',
    precioLista: 320,
  },
  {
    articulo: '0010312',
    descripcion1: 'BALSAMO BLANCO 100G',
    descripcion2: 'BALSAMO BLANCO 100G',
    categoria: 'VETERINARIA',
    familia: null,
    grupo: null,
    claveFabricante: '0010312',
    fabricante: 'NO TIENE',
    unidad: 'pza',
    precioLista: 60,
  },
]

// Pasos 
// 1. Tengo un array y para acceder a el se lo asigno a una variable
// 2. Tengo un array de objetos, encontrar algo que me permita acceder a cada uno de esos elementos (método de array)
// 3. ¿Qué es lo que quiero? - Imprimir en pantalla descripcion1 y precioLista
// 4. Acceder a las propiedades que necesito 

arrayExample.forEach((article)=>{
    const {descripcion1: descripcion, precioLista: precio} = article
    console.log(descripcion, precio)
})

const arryaNames = arrayExample.map((article)=>{
    const {descripcion1, precioLista} = article
    return{
        descripcion1,
        precioLista
    }
})
console.log(arryaNames)

const arrayFilter = arrayExample.filter((article)=>{
    return article.precioLista >70
})

console.log(arrayFilter)