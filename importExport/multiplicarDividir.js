// Exportando varias funciones 

function multiplicar(numeroUno, numeroDos) {
    return numeroUno * numeroDos
}

function dividir(numeroUno, numeroDos) {
    return numeroUno / numeroDos
}

module.exports= {
    multiplicar,
    dividir
}