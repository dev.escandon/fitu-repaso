const resEcommerce = {
  success: true,
  message: 'La información ingresada es correcta ✅',
  payload: {
    jwtToken:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1dWlkQ2xpZW50ZSI6IlpTWGJyUUdweiIsImlhdCI6MTU4NjYzNDAxMiwiZXhwIjoxNTg2NjQ0ODEyfQ.Fnqhyd1hMCaZWA5EFUcdVVuqKA4XP3BGHXPQJ4PCyn0',
    uuidCliente: 'ZSXbrQGpz',
    nombre: 'ACELTEC FERRETERA SA DE CV',
  },
}

// nombreObjeto.propiedad
console.log(resEcommerce.payload)

// const {proiedad} = nombreObjeto

const { jwtToken } = resEcommerce.payload
console.log(jwtToken)
