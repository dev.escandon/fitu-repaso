const arrayNames = ['Noemi', 'Marce', 'Noe', 'Xavi']

console.log(arrayNames[1])

// const [noemi, marce, noe, xavi] = arrayNames
// console.log(marce)
// console.log(noe)
// console.log(noemi)
// console.log(xavi)

var names = ["Cecilie", "Lone"]
var stale = ["Emil", "Tobias", "Linus"]

var children = names.concat(stale)
console.log(children)

const newChildren = [...names, ...stale ]
console.log(newChildren)

const [noemi, marce, ...arrayHombres] = arrayNames
console.log(arrayHombres)

const objPerson ={
    name: 'Daniel',
    apellido: 'Ortega',
    edad: 26,
    tienePerro: true
}

// const {apellido, edad, tienePerro} = objPerson
// const newObj ={
//     apellido,
//     edad,
//     tienePerro
// }
// console.log(newObj)
const {name, ...newArray } = objPerson
console.log(newArray)