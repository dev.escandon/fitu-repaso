const restar = require('./importExport/restar')
const sumar = require('./importExport/sumar')
const { multiplicar, dividir } = require('./importExport/multiplicarDividir')

const resultadoRestar = restar(25, 10)
console.log(resultadoRestar)

const resultadoSumar = sumar(23, 56)
console.log(resultadoSumar)

const resultadoMultiplicar = multiplicar(45, 2)
console.log(resultadoMultiplicar)

const resultadoDividir = dividir(10, 5)
console.log(resultadoDividir)
