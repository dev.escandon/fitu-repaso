const arrayExample = [
    {
      articulo: '0010046',
      descripcion1: 'BROSIN DE 25 GRS',
      descripcion2: 'BROSIN DE 25 GRS',
      categoria: 'VETERINARIA',
      familia: null,
      grupo: null,
      claveFabricante: '0010046',
      fabricante: 'NO TIENE',
      unidad: 'pza',
      precioLista: 90,
    },
    {
      articulo: '0010106',
      descripcion1: 'CARDIOBEE 15 DE 100 ML',
      descripcion2: 'CARDIOBEE 15 100 ML',
      categoria: 'VETERINARIA',
      familia: null,
      grupo: null,
      claveFabricante: '0010106',
      fabricante: 'NO TIENE',
      unidad: 'pza',
      precioLista: 320,
    },
    {
      articulo: '0010312',
      descripcion1: 'BALSAMO BLANCO 100G',
      descripcion2: 'BALSAMO BLANCO 100G',
      categoria: 'VETERINARIA',
      familia: null,
      grupo: null,
      claveFabricante: '0010312',
      fabricante: 'NO TIENE',
      unidad: 'pza',
      precioLista: 60,
    },
  ]
  
  // Imprimir en pantalla descripcion2 y fabricante de cada uno de los artículos
  // Generar un array de cada uno de los artículos desde descripcion2 hasta precioLista

  arrayExample.forEach((article)=>{
    const {descripcion2, fabricante} = article
    console.log(descripcion2, fabricante)
  })
  const newArray = arrayExample.map((article)=>{
    const {articulo, descripcion1, ...restProperties} = article
    return restProperties
  })

  console.log(newArray)

  const newArrayDos = arrayExample.map((article)=>{
    const {descripcion2, fabricante} = article
    console.log(descripcion2, fabricante)
    const {articulo, descripcion1, ...restProperties} = article
    return restProperties
  })
  console.log(newArrayDos)